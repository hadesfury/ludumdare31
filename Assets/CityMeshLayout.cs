﻿using UnityEngine;
using System.Collections;

public class CityMeshLayout : MonoBehaviour
{
    public Transform
        school,
        storage,
        hospital,
        population;

    void Awake()
    {
        school.gameObject.SetActive( false );
        hospital.gameObject.SetActive( false );
    }

    public void BuildSchoolLevel( int school_level, int max_school_level )
    {
        school.gameObject.SetActive( true );
        school.localScale = new Vector3( 0.2f, ( 1.0f / max_school_level ) * school_level, 0.2f );
        school.localPosition = new Vector3( 0.1f, -0.1f * ( max_school_level - school_level ), 0.1f );
    }

    public void BuildHospitalLevel( int hosptital_level, int max_hosptital_level )
    {
        hospital.gameObject.SetActive( true );
        hospital.localScale = new Vector3( 0.4f, ( 0.6f / max_hosptital_level ) * hosptital_level, 0.4f );
        hospital.localPosition = new Vector3( -0.2f, -0.1f * ( max_hosptital_level - hosptital_level ) - 0.2f, -0.2f );
    }

    public void BuildStorageLevel( int storage_level )
    {
        int
            max_visual_storage_level = 4;

        storage.localScale = new Vector3( 0.5f, 0.25f, 0.5f ) * storage_level / ( float ) max_visual_storage_level;
        storage.localPosition = new Vector3( -0.25f, -0.375f - 0.05f * ( storage_level / ( float ) max_visual_storage_level ), 0.25f );
    }
}
