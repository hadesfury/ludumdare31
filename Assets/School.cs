﻿using UnityEngine;
using System.Collections;

public class School : Building
{
    protected int
        maxInstructionLevel = 1;
    protected float
        instructionPower = 0.01f;

    public School()
    {
        buildCost = 20;
        maxUpgradeLevel = 5;
        upgradeCost = 1;
        neededPopulation = 10;
    }

    public override void Build( City city )
    {
        base.Build( city );

        city.cityMeshLayout.BuildSchoolLevel( upgradeLevel, maxUpgradeLevel );
    }

    public override void Upgrade( City city )
    {
        base.Upgrade( city );

        city.cityMeshLayout.BuildSchoolLevel( upgradeLevel, maxUpgradeLevel );
    }

    public override void Update( City city )
    {
        float
            city_education_level = city.GetEducationLevel();

        if( city_education_level <= maxInstructionLevel )
        {
            city_education_level += upgradeLevel * Time.deltaTime * instructionPower;
            city_education_level = Mathf.Min( city_education_level, maxInstructionLevel );

            city.SetEducationLevel( city_education_level );
        }

    }
}
