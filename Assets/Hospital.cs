﻿using UnityEngine;
using System.Collections;

public class Hospital : Building
{
    protected int
        maxHealthLevel = 1;
    protected float
        healthPower = 0.01f;

    public Hospital()
    {
        buildCost = 20;
        maxUpgradeLevel = 3;
        upgradeCost = 1;
        neededPopulation = 20;
    }

    public override void Build( City city )
    {
        base.Build( city );

        city.cityMeshLayout.BuildHospitalLevel( upgradeLevel, maxUpgradeLevel );
    }

    public override void Upgrade( City city )
    {
        base.Upgrade( city );

        city.cityMeshLayout.BuildHospitalLevel( upgradeLevel, maxUpgradeLevel );
    }

    public override void Update( City city )
    {
        float
            health_level = city.GetHealthLevel();

        if( health_level <= maxHealthLevel )
        {
            health_level += upgradeLevel * Time.deltaTime * healthPower;
            health_level = Mathf.Min( health_level, maxHealthLevel );

            city.SetHealthLevel( health_level );
        }
    }
}
