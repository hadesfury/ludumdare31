﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class ShootComponent : MonoBehaviour
{
    public TankController
        tankController;
    public List<AudioClip>
        shootSoundTable;
    public LineRenderer
        laserLine;
    protected int
        damage = 1,
        range = 2;
    protected float
        laserShootDuration = 0.05f,
        hitSoundDelay = 0.2f,
        shootingRate = 0.4f;
    private float
        lastShoot;

    void Awake()
    {
        laserLine.enabled = false;
    }

    void OnTriggerStay( Collider collider )
    {
        EnemyCity
            enemy_city = collider.gameObject.GetComponent<EnemyCity>();

        if( enemy_city != null )
        {
            Shoot( enemy_city );
        }

        //GameObject.Destroy( tank_controller.gameObject );
    }

    private void Shoot( EnemyCity target )
    {
        if( ( Time.time - lastShoot > shootingRate ) && !tankController.GetIsDead() )
        {
            int
                random_sound_index = Random.Range( 0, shootSoundTable.Count - 1 );

            audio.clip = shootSoundTable[ random_sound_index ];
            audio.Play();
            target.Hit( damage, hitSoundDelay );

            tankController.worldManager.worldStat.damageDone += damage;

            laserLine.enabled = true;
            laserLine.SetPosition( 0, transform.position );
            laserLine.SetPosition( 1, target.transform.position );

            lastShoot = Time.time;
        }
        else if( Time.time - lastShoot > laserShootDuration )
        {
            laserLine.enabled = false;
        }
    }

    public void ImproveDamage( int military_fire_power_level )
    {
        damage += military_fire_power_level;
    }

    public void ImproveRange( int military_range_level )
    {
        range += military_range_level;
        transform.localScale = new Vector3( range, 0.5f, range );
    }
}
