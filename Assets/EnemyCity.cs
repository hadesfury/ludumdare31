﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyCity : City
{
    public AudioClip
        hitSound,
        explosionSound;
    public Transform
        areaOfInfluence;
    protected float
        minInfluenceValue = 3,
        influenceFactor = 100,
        birthRate = 4.0f;

    void Awake()
    {
        Initialise( "Enemy City" );
        areaOfInfluence.localScale = Vector3.one;
    }

    public void Hit( int damage_point, float hit_sound_delay )
    {
        population -= damage_point;
        worldManager.worldStat.peopleKilled += damage_point;

        if( ( population <= 0 ) && !isDestroyed )
        {
            worldManager.worldStat.peopleKilled += Mathf.RoundToInt(population);
            audio.clip = explosionSound;
            audio.PlayDelayed( hit_sound_delay );
            isDestroyed = true;
        }
        else if( population > 0 )
        {
            audio.clip = hitSound;
            audio.PlayDelayed( hit_sound_delay );
        }
    }

    void Update()
    {
        population += Time.deltaTime * birthRate;
        resources += Time.deltaTime * /*worldManager.GetProductivity() * */GetCityPopulation();

        areaOfInfluence.localScale = Vector3.one * ( minInfluenceValue + population / influenceFactor );

        if( ( audio.clip != null ) && isDestroyed && !audio.isPlaying )
        {
            GameObject.Destroy( gameObject );
            worldManager.guiLayout.winPanel.gameObject.SetActive( true );
            worldManager.guiLayout.winPanel.GetComponent<ScoreComponent>().DisplayScore();
        }
    }
}
