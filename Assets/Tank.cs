﻿using UnityEngine;
using System.Collections;

public class Tank : Resource
{
    protected int
        buildCost = 20,
        neededPopulation = 10;
    protected bool
        hasBeenUnlocked = false;

    public override bool CanBeConstructed( City city )
    {
        bool
            can_be_constructed = ( ( city.GetCityPopulation() > neededPopulation ) && ( city.GetCityResources() > GetBuildCost( city ) ) );

        if( can_be_constructed )
        {
            hasBeenUnlocked = true;
        }

        return can_be_constructed || hasBeenUnlocked;
    }

    public override void Build( City city )
    {
        int
            tank_count = city.GetTankCount();

        ++resourceCount;
        ++tank_count;
        city.SpendCityResources( GetBuildCost( city ) );
        city.SetTankCount( tank_count );
        city.LaunchTank();
        ++city.worldManager.worldStat.buildTankCount;
    }

    public override int GetBuildCost( City city )
    {
        return buildCost - city.worldManager.GetMilitaryCostReduction();
    }
}
