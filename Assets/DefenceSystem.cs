﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class DefenceSystem : MonoBehaviour
{
    public List<AudioClip>
        shootSoundTable;
    public LineRenderer
        laserLine;
    protected int
        cityDamage = 10,
        damage = 1;
    protected float
        laserShootDuration = 0.05f,
        hitSoundDelay = 0.1f,
        shootingRate = 0.4f;
    private float
        lastShoot;

    void Awake()
    {
        laserLine.enabled = false;
    }

    void OnTriggerStay( Collider collider )
    {
        TankController
            tank_controller = collider.gameObject.GetComponent<TankController>();

        if( tank_controller != null ) 
        {
            Shoot( tank_controller );
        }

        //GameObject.Destroy( tank_controller.gameObject );
    }

    private void Shoot( TankController target )
    {
        if( ( Time.time - lastShoot > shootingRate ) && !target.GetIsDead() )
        {
            int
                random_sound_index = Random.Range( 0, shootSoundTable.Count - 1 );

            audio.clip = shootSoundTable[ random_sound_index ];
            audio.Play();
            target.Hit( hitSoundDelay );

            laserLine.enabled = true;
            laserLine.SetPosition( 0, transform.position );
            laserLine.SetPosition( 1, target.transform.position );

            lastShoot = Time.time;
        }
        else if( Time.time - lastShoot > laserShootDuration )
        {
            laserLine.enabled = false;
        }
    }

    public void Attack( City city )
    {
        Shoot( city );
    }

    private void Shoot( City target )
    {
        if( ( Time.time - lastShoot > shootingRate ) && !target.GetIsDetroyed() )
        {
            int
                random_sound_index = Random.Range( 0, shootSoundTable.Count - 1 );

            audio.clip = shootSoundTable[ random_sound_index ];
            audio.Play();
            target.DealDamage( cityDamage );

            laserLine.enabled = true;
            laserLine.SetPosition( 0, transform.position );
            laserLine.SetPosition( 1, target.transform.position );

            lastShoot = Time.time;
        }
        else if( Time.time - lastShoot > laserShootDuration )
        {
            laserLine.enabled = false;
        }
    }
}
