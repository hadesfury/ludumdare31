﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GuiLayout : MonoBehaviour
{
    public Button
        buildCityButton,
        improveProductivityButton,
        improveMilitaryCostButton,
        improveRangeButton,
        improveArmorButton,
        improveFirepowerButton;
    public WorldStatPanelLayout
        worldStatPanel;
    public Image
        enemyCastleStatPanel,
        statParentPanel,
        cityContentPanel,
        losePanel,
        winPanel;

    void Awake()
    {
        improveProductivityButton.gameObject.SetActive( false );
        improveMilitaryCostButton.gameObject.SetActive( false );
        improveRangeButton.gameObject.SetActive( false );
        improveArmorButton.gameObject.SetActive( false );
        improveFirepowerButton.gameObject.SetActive( false );

        winPanel.gameObject.SetActive( false );
        losePanel.gameObject.SetActive( false );

    }
}
