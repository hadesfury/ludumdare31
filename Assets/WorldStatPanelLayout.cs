﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WorldStatPanelLayout : MonoBehaviour
{
    public Text
        populationText,
        resourcesText,
        productivityText,
        timerText;

    public void UpdateData( int population, int resources, float productivity, float time )
    {
        populationText.text = String.Format( "World Population : {0}", population );
        //storageText.text = String.Format("World Storage : {0}", storage * storage);
        productivityText.text = String.Format( "World Productivity : {0:0.00}%", productivity );
        resourcesText.text = String.Format( "World Resources : {0}", resources );

        timerText.text = String.Format( "{0:00}:{1:00}", Mathf.FloorToInt( time / 60.0f ), time % 60 );
    }
}
