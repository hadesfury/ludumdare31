﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldManager : MonoBehaviour
{
    public class WorldStat
    {
        public int
            buildTankCount = 0,
            damageDone = 0,
            damageTaken = 0,
            peopleKilled = 0;
        public float
            resourceProduced = 0;
    }

    public WorldStat
        worldStat = new WorldStat();
    public bool
        debugMode = false;
    public GroundInteractionComponent
        ground;
    public EnemyCity
        enemyCastleObject;
    public GuiLayout
        guiLayout;

    public City
        cityObjectPrefab;
    public Button
        cityButtonPrefab;

    protected int
        resourceNeededForNewCity = 80,
        militaryFirePowerLevel = 0,
        militaryRangeLevel = 0,
        militaryArmorLevel = 0,
        militaryReductionLevel = 0,
        impoveRangeLock = 100,
        impoveArmorLock = 10,
        impoveFirepowerLock = 15,
        improveMilitaryCostLock = 10,
        maxCityCount = 10;
    protected float
        educationReductionStartValue = 25,
        militaryFirePowerTuningValue = 2,
        militaryRangeTuningValue = 1,
        militaryArmorTuningValue = 3,
        militaryReductionTuningValue = 4,
        initialProductivity = 0.02f;

    protected City
        selectedCity;
    private readonly List<City>
        cityTable = new List<City>();

    private float
        timeElapsed,
        productivity;
    private int
        nextUpgradeCost = 0,
        productivityLevel = 1,
        //productivityLevelCost = 1,
        worldPopulation,
        worldResources,
        cityIndex = 0;

    void Start()
    {
#if ( UNITY_EDITOR_WIN )
        {
            //debugMode = true;
            Time.timeScale = 4;
        }
#endif
        productivity = initialProductivity;
        guiLayout.winPanel.gameObject.SetActive( false );
    }

    public void ResetTime()
    {
        timeElapsed = 0;
    }

    public int GetWorldResources()
    {
        return worldResources;
    }

    public int GetMilitaryCostReduction()
    {
        return Mathf.RoundToInt( militaryReductionLevel * militaryReductionTuningValue );
    }

    public int GetMilitaryArmorLevel()
    {
        return Mathf.RoundToInt( militaryArmorLevel * militaryArmorTuningValue );
    }

    public int GetMilitaryFirePowerLevel()
    {
        return Mathf.RoundToInt(militaryFirePowerLevel * militaryFirePowerTuningValue);
    }

    public int GetMilitaryRangeLevel()
    {
        return Mathf.RoundToInt( militaryRangeLevel * militaryRangeTuningValue );
    }

    public float GetProductivity()
    {
        return productivity * productivityLevel;
    }

    public int GetNextUpgradeCost()
    {
        return Mathf.RoundToInt( nextUpgradeCost * GetEducationReduction() );
    }

    public float GetEducationReduction()
    {
        int
            education_reduction = 0,
            city_count = 0;

        foreach( City current_city in cityTable )
        {
            ++city_count;

            education_reduction += Mathf.RoundToInt( educationReductionStartValue * current_city.GetEducationLevel() / city_count );
        }

        return 1 - ( education_reduction / 100.0f );//Mathf.Max(1,100 - education_reduction);
    }

    public void ChooseCityPosition()
    {
        if( cityIndex < maxCityCount )
        {
            ground.ChooseCityPosition( BuildCity );
        }
        else
        {
            guiLayout.winPanel.gameObject.SetActive( true );
            guiLayout.winPanel.GetComponent<ScoreComponent>().DisplayScore();
        }
    }

    private void BuildCity( Vector3 city_position )
    {
        if( cityIndex < maxCityCount )
        {
            if( GetWorldResources() >= GetNextUpgradeCost() )
            {
                Button
                    city_button = ( Button ) GameObject.Instantiate( cityButtonPrefab );
                City
                    city_object = ( City ) GameObject.Instantiate( cityObjectPrefab );
                string
                    current_city_name = "City #" + cityIndex;

                city_button.transform.SetParent( guiLayout.cityContentPanel.transform, false );
                city_button.GetComponentInChildren<Text>().text = current_city_name;
                city_button.onClick.AddListener( city_object.OnCitySelected );
                city_object.gameObject.name = current_city_name;
                city_object.worldManager = this;
                city_object.Initialise( current_city_name );
                city_object.transform.position = city_position;
                cityTable.Add( city_object );
                SelectCity( city_object );
                SpendWorldResources( GetNextUpgradeCost() );
                nextUpgradeCost *= 2;

                if( cityIndex == 0 )
                {
                    nextUpgradeCost = 1;
                }

                ++cityIndex;
            }
        }
        else
        {
            guiLayout.winPanel.gameObject.SetActive( true );
            guiLayout.winPanel.GetComponent<ScoreComponent>().DisplayScore();
        }
    }

    public void Restart()
    {
        Application.LoadLevel( 0 );
    }

    public void SelectEnemyCastle()
    {
        SelectCity( null );
        guiLayout.enemyCastleStatPanel.gameObject.SetActive( true );
    }

    public void SelectCity( City city_to_select )
    {
        if( selectedCity != null )
        {
            selectedCity.HideStat();
            selectedCity.statPanel.gameObject.SetActive( false );
        }
        selectedCity = city_to_select;
        guiLayout.enemyCastleStatPanel.gameObject.SetActive( false );
        if( selectedCity != null )
        {
            selectedCity.ShowStat();
        }
    }

    public City GetSelectedCity()
    {
        return selectedCity;
    }

    public float GetTimeElapsed()
    {
        return timeElapsed;
    }

    public void IncreaseProductivity()
    {
        if( GetWorldResources() >= GetNextUpgradeCost() )
        {
            SpendWorldResources( GetNextUpgradeCost() );
            ++productivityLevel;
            nextUpgradeCost *= 2;
        }
    }

    public void ReduceMilitaryCost()
    {
        if( GetWorldResources() >= GetNextUpgradeCost() )
        {
            SpendWorldResources( GetNextUpgradeCost() );
            ++militaryReductionLevel;
            nextUpgradeCost *= 2;
        }
    }

    public void IncreaseArmor()
    {
        if( GetWorldResources() >= GetNextUpgradeCost() )
        {
            SpendWorldResources( GetNextUpgradeCost() );
            ++militaryArmorLevel;
            nextUpgradeCost *= 2;
        }
    }

    public void IncreaseFirepower()
    {
        if( GetWorldResources() >= GetNextUpgradeCost() )
        {
            SpendWorldResources( GetNextUpgradeCost() );
            ++militaryFirePowerLevel;
            nextUpgradeCost *= 2;
        }
    }

    void Update()
    {
        worldPopulation = 0;
        worldResources = 0;

        foreach( City current_city in cityTable )
        {
            worldPopulation += current_city.GetCityPopulation();
            worldResources += current_city.GetCityResources();
        }

        timeElapsed += Time.deltaTime;

        guiLayout.worldStatPanel.UpdateData( worldPopulation, worldResources, GetProductivity(), timeElapsed );

        if( cityIndex > 0 )
        {
            UpdateGenericButton( guiLayout.improveRangeButton, "Improve range", worldPopulation > impoveRangeLock );
            UpdateGenericButton( guiLayout.improveArmorButton, "Improve armor", worldStat.damageTaken > impoveArmorLock );
            UpdateGenericButton( guiLayout.improveFirepowerButton, "Improve fire power", worldStat.damageDone > impoveFirepowerLock );
            UpdateGenericButton( guiLayout.improveProductivityButton, "Increase Productivity", true );
            UpdateGenericButton( guiLayout.improveMilitaryCostButton, "Reduce Military Cost", worldStat.buildTankCount > improveMilitaryCostLock );
        }
        UpdateBuildCityButton();
    }

    private void UpdateGenericButton( Button button, string button_label, bool condition )
    {
        if( condition )
        {
            button.gameObject.SetActive( true );
            button.GetComponentInChildren<Text>().text = String.Format( button_label + " ( Cost {0} )", GetNextUpgradeCost() );

            button.interactable = false;

            if( worldResources >= GetNextUpgradeCost() )
            {
                button.interactable = true;
            }
        }
        else
        {
            button.gameObject.SetActive( false );
        }
    }

    private void UpdateBuildCityButton()
    {
        if( worldResources >= resourceNeededForNewCity * cityIndex )
        {
            guiLayout.buildCityButton.gameObject.SetActive( true );
            guiLayout.buildCityButton.GetComponentInChildren<Text>().text = String.Format( "Build City ( Cost {0} )", GetNextUpgradeCost() );

            guiLayout.buildCityButton.interactable = false;

            if( worldResources >= GetNextUpgradeCost() )
            {
                guiLayout.buildCityButton.interactable = true;
            }
        }
        else
        {
            guiLayout.buildCityButton.interactable = false;
            guiLayout.buildCityButton.gameObject.SetActive( false );
        }
    }

    public void SpendWorldResources( int resource_cost )
    {
        int
            resource_left = resource_cost;

        while( resource_left > 0 )
        {
            foreach( City current_city in cityTable )
            {
                if( resource_left <= 0 )
                {
                    break;
                }
                else if( current_city.GetCityResources() > 0 )
                {
                    current_city.SpendCityResources( 1 );
                    --resource_left;
                }
            }
        }
    }

    public void CheckLoseCondition()
    {
        bool
            has_undestructed_city = false;

        foreach( City city in cityTable )
        {
            if( !city.GetIsDetroyed() )
            {
                has_undestructed_city = true;
                break;
                ;
            }
        }

        if( !has_undestructed_city )
        {
            guiLayout.losePanel.gameObject.SetActive( true );
            guiLayout.losePanel.GetComponent<ScoreComponent>().DisplayScore();
        }
    }
}