﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreComponent : MonoBehaviour
{
    public WorldManager
        worldManager;
    public Text
        scoreText;

    public void DisplayScore()
    {
        //Time.timeScale = 0;
        float
            best_time = PlayerPrefs.GetFloat( "time", 0 ),
            best_resources = PlayerPrefs.GetFloat( "resources", 0 ),
            time = worldManager.GetTimeElapsed();
        int
            best_killed = PlayerPrefs.GetInt( "killed", 0 );
        string
            score_text = String.Format( "Your time : {0:00}:{1:00} ( Last Best : {2:00}:{3:00} ) \n", Mathf.FloorToInt( time / 60.0f ), time % 60, Mathf.FloorToInt( best_time / 60.0f ), best_time % 60 );

        score_text += String.Format( "People killed : {0} ( Last Best : {1} )\n", worldManager.worldStat.peopleKilled, best_killed );
        score_text += String.Format( "Resources produced : {0:0} ( Last Best : {1:0} )\n", worldManager.worldStat.resourceProduced, best_resources );
        
        scoreText.text = score_text;

        if( time < best_time )
        {
            PlayerPrefs.SetFloat( "time", time );
        }

        if( worldManager.worldStat.peopleKilled > best_killed )
        {
            PlayerPrefs.SetInt( "killed", worldManager.worldStat.peopleKilled );
        }

        if( worldManager.worldStat.resourceProduced > best_resources )
        {
            PlayerPrefs.SetFloat( "resources", worldManager.worldStat.resourceProduced );
        }
    }

}
