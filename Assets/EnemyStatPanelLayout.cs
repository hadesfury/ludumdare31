﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyStatPanelLayout : MonoBehaviour
{
    public WorldManager 
        worldManager;

    public Text cityName,
        populationText,
        resourcesText,
        educationText,
        healthText,
        tankCountText;
    public EnemyCity
        enemyCity;

    void Update()
    {
        EnemyCity
            current_city = enemyCity;

        cityName.text = current_city.GetCityName();
        populationText.text = String.Format( "Population : {0}", current_city.GetCityPopulation() );
        resourcesText.text = String.Format( "Resources : {0}", current_city.GetCityResources() );

        if( current_city.GetEducationLevel() > 0 )
        {
            educationText.gameObject.SetActive( true );
            educationText.text = String.Format( "Education : {0:0.00}", current_city.GetEducationLevel() );
        }
        else
        {
            educationText.gameObject.SetActive( false );
        }

        if( current_city.GetHealthLevel() > 0 )
        {
            healthText.gameObject.SetActive( true );
            healthText.text = String.Format( "Health : {0:0.00}", current_city.GetHealthLevel() );
        }
        else
        {
            healthText.gameObject.SetActive( false );
        }

        if( current_city.GetTankCount() > 0 )
        {
            tankCountText.gameObject.SetActive( true );
            tankCountText.text = String.Format( "Tank Count : {0}", current_city.GetTankCount() );
        }
        else
        {
            tankCountText.gameObject.SetActive( false );
        }
    }
}