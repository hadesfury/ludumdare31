﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public abstract class Building
{
    public
        Button button;
    protected bool
        isBuilt = false;
    protected int
        upgradeLevel = 1,
        buildCost = 20,
        maxUpgradeLevel = 10,
        upgradeCost = 1,
        neededPopulation = 20;
        
    public abstract void Update( City city );

    public bool GetIsBuilt()
    {
        return isBuilt;
    }

    public int GetBuildCost()
    {
        return buildCost;
    }

    public int GetUpgradeCost()
    {
        return upgradeCost;
    }

    public int GetMaxUpgradeLevel()
    {
        return maxUpgradeLevel;
    }

    public bool CanBeConstructed( City city )
    {
        bool
            can_be_constructed = ( ( city.GetCityPopulation() > neededPopulation ) && ( city.GetCityResources() > buildCost ) );

        return can_be_constructed;
    }

    public bool CanBeUpgraded()
    {
        return upgradeLevel < maxUpgradeLevel;
    }

    public virtual void Build( City city )
    {
        isBuilt = true;
        city.SpendCityResources( buildCost );
    }

    public virtual void Upgrade( City city )
    {
        ++upgradeLevel;
        city.SpendCityResources( upgradeCost );
        upgradeCost *= 2;
    }
}
