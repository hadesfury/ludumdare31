﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class City : MonoBehaviour
{
    public CityMeshLayout
        cityMeshLayout;
    public WorldManager
        worldManager;
    public CityStatPanelLayout
        statPanelPrefab,
        statPanel;
    public TankController
        tankPrefab;

    protected int
        initialStorage = 10,
        storageLevelCost = 1;
    protected string
        cityName;

    protected bool
        isDestroyed = false;
    protected float
        healthReductionStartValue = 25,
        population = 0,
        resources = 0;
    private float
        ownership = -50,
        educationLevel = 0,
        healthLevel = 0;
    private int
        storageLevel = 1,
        storage,
        tankCount = 0;

    private readonly List<Building>
        buildingTable = new List<Building>();
    private readonly List<Resource>
        resourceTable = new List<Resource>();

    void Awake()
    {
        buildingTable.Add( new School() );
        buildingTable.Add( new Hospital() );

        resourceTable.Add( new Tank() );
    }

    public void Initialise( string city_name )
    {
        storage = initialStorage;
        cityName = city_name;
    }

    public int GetTankCount()
    {
        return tankCount;
    }

    public void SetTankCount( int tank_count )
    {
        tankCount = tank_count;
    }

    public void LaunchTank()
    {
        TankController
            tank_controller = ( TankController ) GameObject.Instantiate( tankPrefab );

        tank_controller.worldManager = worldManager;
        tank_controller.ImproveRange( worldManager.GetMilitaryRangeLevel() );
        tank_controller.ImproveFirePower( worldManager.GetMilitaryFirePowerLevel() );
        tank_controller.ImproveArmor( worldManager.GetMilitaryArmorLevel() );

        tank_controller.target = worldManager.enemyCastleObject.transform;
        tank_controller.transform.position = transform.position;

        --tankCount;
    }

    public void LaunchMissile()
    {

    }

    public string GetCityName()
    {
        return cityName;
    }

    public int GetStorageLevelCost()
    {
        return Mathf.RoundToInt( storageLevelCost * GetHealthReduction() );
    }

    public void OnCitySelected()
    {
        worldManager.SelectCity( this );
    }

    public float GetHealthReduction()
    {
        int
            health_reduction = 0;

        health_reduction += Mathf.RoundToInt( healthReductionStartValue * GetHealthLevel() );

        return 1 - ( health_reduction / 100.0f );
    }


    private void Update()
    {
        EnemyCity
            enemy_city = worldManager.enemyCastleObject;
        float
            resource_produced = Time.deltaTime * worldManager.GetProductivity() * GetCityPopulation(),
            resource_excess;

        if( worldManager.debugMode )
        {
            population += Time.deltaTime * 100;
        }
        else
        {
            population += Time.deltaTime;
        }

        resources += resource_produced;
        resource_excess = resources - GetCityStorage();
        resources = Mathf.Min( resources, GetCityStorage() );

        worldManager.worldStat.resourceProduced += Mathf.Min( resource_produced, resource_produced - resource_excess );

        if( enemy_city != null )
        {
            float
                distance_to_enemy_city = Vector3.Distance( enemy_city.transform.position, transform.position );

            if( distance_to_enemy_city < enemy_city.areaOfInfluence.localScale.x / 2.0f )
            {
                enemy_city.areaOfInfluence.GetComponent<DefenceSystem>().Attack( this );
            }
        }
    }

    //public void SetOwnerShip( float ownership )
    //{
    //    this.ownership = ownership;
    //}

    //private void ForceCulturalChange( float cultural_influence )
    //{
    //    if( ownership > 0 )
    //    {

    //    }

    //}

    //public float GetCulturalPower()
    //{
    //    return 0;
    //}

    public int GetCityPopulation()
    {
        return Mathf.RoundToInt( population );
    }

    public int GetCityResources()
    {
        return Mathf.RoundToInt( resources );
    }

    public int GetCityStorage()
    {
        return storage * storageLevel;
    }

    public void BuildTank()
    {
        //if( worldManager.GetWorldResources() >= tankCost )
        //{
        //    worldManager.SpendWorldResources( tankCost );
        //    ++tankCount;
        //} 
    }

    public void IncreaseStorage()
    {
        if( GetCityResources() >= GetStorageLevelCost() )
        {
            SpendCityResources( GetStorageLevelCost() );
            ++storageLevel;
            storageLevelCost *= 2;
            storageLevelCost = Mathf.Min( GetStorageLevelCost(), GetCityStorage() );
        }
    }

    public void SpendCityResources( int resource_cost )
    {
        resources -= resource_cost;
    }

    public List<Building> GetBuildingTable()
    {
        return buildingTable;
    }

    public List<Resource> GetResourceTable()
    {
        return resourceTable;
    }

    public float GetEducationLevel()
    {
        return educationLevel;
    }

    public void SetEducationLevel( float education_level )
    {
        educationLevel = education_level;
    }

    public float GetHealthLevel()
    {
        return healthLevel;
    }

    public void SetHealthLevel( float health_level )
    {
        healthLevel = health_level;
    }

    public void ShowStat()
    {
        if( statPanel == null )
        {
            statPanel = ( CityStatPanelLayout ) GameObject.Instantiate( statPanelPrefab );
            statPanel.worldManager = worldManager;
            statPanel.transform.SetParent( worldManager.guiLayout.statParentPanel.transform, false );
        }

        statPanel.gameObject.SetActive( true );
    }

    public void HideStat()
    {
        if( statPanel != null )
        {
            statPanel.gameObject.SetActive( false );
        }
    }

    public void DealDamage( int damage )
    {
        population -= damage;

        if( population <= 0 )
        {
            gameObject.SetActive( false );
            isDestroyed = true;
            worldManager.CheckLoseCondition();
        }
    }

    public bool GetIsDetroyed()
    {
        return isDestroyed;
    }
}
