﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class InteractionComponent : MonoBehaviour
{
    public UnityEvent
        onLeftClickEvent;

    void Update()
    {
        if( Input.GetMouseButtonUp( 0 ) )
        {
            Ray
                current_ray = Camera.main.ScreenPointToRay( Input.mousePosition );
            RaycastHit
                raycast_hit;

            if( Physics.Raycast( current_ray, out raycast_hit ) && ( raycast_hit.collider.gameObject == gameObject ) )
            {
                OnLeftClick();
            }
        }
        else if( Input.GetMouseButtonUp( 1 ) )
        {
            Ray
                current_ray = Camera.main.ScreenPointToRay( Input.mousePosition );
            RaycastHit
                raycast_hit;

            if( Physics.Raycast( current_ray, out raycast_hit ) && ( raycast_hit.collider.gameObject == gameObject ) )
            {
                OnRightClick();
            }
        }
    }

    private void OnLeftClick()
    {
        //print("OnLeftClick");
        if( onLeftClickEvent != null )
        {
            onLeftClickEvent.Invoke();
        }
    }

    private void OnRightClick()
    {
        //print("OnRightClick");
    }
}
