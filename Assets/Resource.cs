﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public abstract class Resource
{
    public Button
        button;
    protected int
        resourceCount = 0;

    public abstract bool CanBeConstructed( City city );
    public abstract void Build( City city );
    public abstract int GetBuildCost( City city );
}
