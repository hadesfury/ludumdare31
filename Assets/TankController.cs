﻿using UnityEngine;
using System.Collections;

public class TankController : MonoBehaviour
{
    public WorldManager
        worldManager;
    public ParticleSystem
        explosionParticleSystem;
    public Transform
        target;
    public AudioClip
        hitSound,
        explosionSound;
    protected int
        lifePoint = 3;
    protected float
        range = 2.5f;

    private bool
        isDead = false;

    void Awake()
    {
        explosionParticleSystem.gameObject.SetActive( false );
    }

    public bool GetIsDead()
    {
        return isDead;
    }

    public void Hit( float hit_sound_delay )
    {
        --lifePoint;
        ++worldManager.worldStat.damageTaken;

        if( lifePoint == 0 )
        {
            audio.clip = explosionSound;
            audio.PlayDelayed( hit_sound_delay );
            explosionParticleSystem.gameObject.SetActive( true );
            isDead = true;

        }
        else if( lifePoint > 0 )
        {
            audio.clip = hitSound;
            audio.PlayDelayed( hit_sound_delay );
        }
    }

    void Update()
    {
        if( target != null )
        {
            NavMeshAgent
                nav_mesh_agent = GetComponent<NavMeshAgent>();

            if( ( Vector3.Distance( transform.position, target.position ) > range ) && !GetIsDead() )
            {
                nav_mesh_agent.SetDestination( target.position );
            }
            else
            {
                nav_mesh_agent.Stop();
            }
        }

        if( isDead && ( audio.clip != null ) && !audio.isPlaying )
        {
            GameObject.Destroy( gameObject );
        }
    }

    public void ImproveRange( int military_range_level )
    {
        GetComponentInChildren<ShootComponent>().ImproveRange( military_range_level );
    }

    public void ImproveFirePower( int military_fire_power_level )
    {
        GetComponentInChildren<ShootComponent>().ImproveDamage( military_fire_power_level );
    }

    public void ImproveArmor( int military_armor_level )
    {
        lifePoint += military_armor_level;
    }
}
