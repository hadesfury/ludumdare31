﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CityStatPanelLayout : MonoBehaviour
{
    public WorldManager
        worldManager;

    public Text cityName,
        populationText,
        resourcesText,
        storageText,
        educationText,
        healthText,
        tankCountText;
    public Button
        storageLevelIncreaseButton,
        buttonPrefab,
        launchTankButton;

    void Awake()
    {
        tankCountText.gameObject.SetActive( false );
        launchTankButton.gameObject.SetActive( false );
    }

    void Update()
    {
        City
            current_city = worldManager.GetSelectedCity();

        cityName.text = current_city.GetCityName();
        populationText.text = String.Format( "Population : {0}", current_city.GetCityPopulation() );
        storageText.text = String.Format( "Storage : {0}", current_city.GetCityStorage() );
        resourcesText.text = String.Format( "Resources : {0}", current_city.GetCityResources() );

        if( current_city.GetEducationLevel() > 0 )
        {
            educationText.gameObject.SetActive( true );
            educationText.text = String.Format( "Education : {0:0.00}", current_city.GetEducationLevel() );
        }
        else
        {
            educationText.gameObject.SetActive( false );
        }

        if( current_city.GetHealthLevel() > 0 )
        {
            healthText.gameObject.SetActive( true );
            healthText.text = String.Format( "Health : {0:0.00}", current_city.GetHealthLevel() );
        }
        else
        {
            healthText.gameObject.SetActive( false );
        }

        tankCountText.text = String.Format( "Tank Count : {0}", current_city.GetTankCount() );
        if( current_city.GetTankCount() > 0 )
        {
            tankCountText.gameObject.SetActive( true );
            //launchTankButton.gameObject.SetActive( true );
            //launchTankButton.interactable = true;
        }
        else
        {
            //launchTankButton.interactable = false;
        }

        storageLevelIncreaseButton.GetComponentInChildren<Text>().text = String.Format( "Increase Storage ( Cost {0} )", current_city.GetStorageLevelCost() );

        storageLevelIncreaseButton.interactable = false;

        if( current_city.GetCityResources() >= current_city.GetStorageLevelCost() )
        {
            storageLevelIncreaseButton.interactable = true;
        }

        foreach( Building current_building in current_city.GetBuildingTable() )
        {
            if( current_building.button == null )
            {
                if( current_building.CanBeConstructed( current_city ) )
                {
                    Button
                        new_button = ( Button ) GameObject.Instantiate( buttonPrefab );
                    Building
                        building = current_building;

                    current_building.button = new_button;
                    new_button.interactable = false;
                    new_button.onClick.AddListener( () => Build( building, current_city ) );
                    new_button.transform.SetParent( GetComponent<ScrollRect>().content.transform, false );
                }
            }
            else if( !current_building.GetIsBuilt() )
            {
                current_building.button.GetComponentInChildren<Text>().text = String.Format( "Build {0} ( Cost {1} )", current_building.GetType(), current_building.GetBuildCost() );

                if( current_building.GetBuildCost() <= current_city.GetCityResources() )
                {
                    current_building.button.interactable = true;
                }
                else
                {
                    current_building.button.interactable = false;
                }
            }
            else
            {
                current_building.Update( current_city );
                if( current_building.CanBeUpgraded() )
                {
                    current_building.button.GetComponentInChildren<Text>().text = String.Format(
                        "Upgrade {0} ( Cost {1} )",
                        current_building.GetType(),
                        current_building.GetUpgradeCost() );

                    if( current_building.GetUpgradeCost() <= current_city.GetCityResources() )
                    {
                        current_building.button.interactable = true;
                    }
                    else
                    {
                        current_building.button.interactable = false;
                    }
                }
                else
                {
                    current_building.button.GetComponentInChildren<Text>().text = String.Format(
                        "Upgrade {0} ( Max )",
                        current_building.GetType() );
                    current_building.button.interactable = false;
                }
            }
        }

        foreach( Resource current_resource in current_city.GetResourceTable() )
        {
            if( current_resource.button == null )
            {
                if( current_resource.CanBeConstructed( current_city ) )
                {
                    Button
                        new_button = ( Button ) GameObject.Instantiate( buttonPrefab );
                    Resource
                        resource = current_resource;

                    current_resource.button = new_button;
                    new_button.interactable = false;
                    new_button.onClick.AddListener( () => BuildResource( resource, current_city ) );
                    new_button.transform.SetParent( GetComponent<ScrollRect>().content.transform, false );
                }
            }
            else
            {
                current_resource.button.GetComponentInChildren<Text>().text = String.Format( "Build  {0} ( Cost {1} )", current_resource.GetType(), current_resource.GetBuildCost( current_city ) );

                if( current_resource.GetBuildCost( current_city ) <= current_city.GetCityResources() )
                {
                    current_resource.button.interactable = true;
                }
                else
                {
                    current_resource.button.interactable = false;
                }
            }
        }

    }

    private void BuildResource( Resource current_resource, City current_city )
    {
        current_resource.Build( current_city );
    }

    private void Build( Building current_building, City current_city )
    {
        current_building.button.interactable = false;
        current_building.Build( current_city );
        current_building.button.onClick.RemoveAllListeners();
        current_building.button.onClick.AddListener( () => Upgrade( current_building, current_city ) );
    }

    private void Upgrade( Building current_building, City current_city )
    {
        current_building.Upgrade( current_city );
    }

    public void IncreaseStorage()
    {
        City
            current_city = worldManager.GetSelectedCity();

        current_city.IncreaseStorage();
    }

    public void LaunchTank()
    {
        City
            current_city = worldManager.GetSelectedCity();

        current_city.LaunchTank();
    }
}