﻿using System;
using UnityEngine;

public class GroundInteractionComponent : MonoBehaviour
{
    public GameObject
        cursor;
    private event Action<Vector3>
        onLeftClickEvent;

    void Awake()
    {
        cursor.SetActive( false );
    }

    private void Update()
    {
        if( cursor.activeSelf )
        {
            Ray
                current_ray = Camera.main.ScreenPointToRay( Input.mousePosition );
            RaycastHit
                raycast_hit;

            if( Physics.Raycast( current_ray, out raycast_hit )
                && ( raycast_hit.collider.gameObject == gameObject ) )
            {
                int
                    x_coordinate = Mathf.FloorToInt( raycast_hit.point.x ),
                    z_coordinate = Mathf.FloorToInt( raycast_hit.point.z );
                Vector3
                    cursor_position = Vector3.one * 0.5f;

                cursor_position.x += x_coordinate;
                cursor_position.z += z_coordinate;

                if( Input.GetMouseButtonDown( 0 ) )
                {
                    OnLeftClick( cursor_position );
                }
                else if( Input.GetMouseButtonDown( 1 ) || Input.GetMouseButtonUp( 1 ) )
                {
                    onLeftClickEvent = null;
                    cursor.SetActive( false );
                }
                else
                {
                    cursor.transform.position = cursor_position;
                }
            }
        }
    }

    private void OnLeftClick( Vector3 point )
    {
        if( onLeftClickEvent != null )
        {
            onLeftClickEvent( point );
            onLeftClickEvent = null;
            cursor.SetActive( false );
        }

    }

    private void OnRightClick( Vector3 point )
    {
        int
            x_coordinate = Mathf.FloorToInt( point.x ),
            z_coordinate = Mathf.FloorToInt( point.z );

        print( "OnRightClick : " + point + " " + x_coordinate + "," + z_coordinate );
    }

    public void ChooseCityPosition( Action<Vector3> callback )
    {
        if( !cursor.activeSelf )
        {
            onLeftClickEvent += callback;
            cursor.SetActive( true );
        }
    }
}